
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('DATA0008.csv')

h, g, j = ['Phase A RMS Current', 'Phase B RMS Current', 'Phase C RMS Current']

c = df.apply(lambda x: x[h] != 0 or x[g] != 0 or x[j] != 0, axis=1)
b = df[c][[h, g, j]]

a = b.mean()
c = b[b != 0].min()
f = b.max()


print(f" mean :")
print(a)
print(f"\n min :")
print(c)
print(f"\n max:")
print(f)
b.plot(subplots=True, figsize=(10, 8), title='The Graph')
plt.show()
